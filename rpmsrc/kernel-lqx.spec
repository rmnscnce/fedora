Name: kernel-lqx
Summary: The Linux Kernel with Liquorix Patches
Version: 5.11.1
Release: lqx1%{?dist}
%define kbuildver %{version}-%{release}
License: GPLv2 and Redistributable, no modifications permitted
Group: System Environment/Kernel
Vendor: The Linux Community
URL: https://www.kernel.org
Source: kernel-lqx-%{version}.tar.gz
Provides:  kernel-lqx-%{kbuildver}
%define __spec_install_post /usr/lib/rpm/brp-compress || :
%define debug_package %{nil}
BuildRequires: python3-devel gcc make perl-generators perl-interpreter openssl-devel bison flex findutils git-core perl-devel openssl elfutils-devel gawk binutils m4 tar hostname bzip2 bash gzip xz bc diffutils redhat-rpm-config net-tools elfutils patch rpm-build dwarves kmod libkcapi-hmaccalc perl-Carp

%description
The Linux Kernel, the operating system core itself

%package headers
Summary: Header files for the Linux kernel for use by glibc
Group: Development/System
Provides: %{name}-headers = %{kbuildver}
%description headers
Kernel-headers includes the C header files that specify the interface
between the Linux kernel and userspace libraries and programs.  The
header files define structures and constants that are needed for
building most standard programs and are also needed for rebuilding the
glibc package.

%package devel
Summary: Development package for building kernel modules to match the %{kbuildver} kernel
Group: System Environment/Kernel
AutoReqProv: no
%description -n %{name}-devel
This package provides kernel headers and makefiles sufficient to build modules
against the %{kbuildver} kernel package.

%prep
%setup -q

%build
make %{?_smp_mflags} KBUILD_BUILD_VERSION=%{kbuildver}

%install
mkdir -p %{buildroot}/boot
%ifarch ia64
mkdir -p %{buildroot}/boot/efi
cp $(make image_name) %{buildroot}/boot/efi/vmlinuz-%{kbuildver}
ln -s efi/vmlinuz-%{kbuildver} %{buildroot}/boot/
%else
cp $(make image_name) %{buildroot}/boot/vmlinuz-%{kbuildver}
%endif
make %{?_smp_mflags} INSTALL_MOD_PATH=%{buildroot} modules_install
make %{?_smp_mflags} INSTALL_HDR_PATH=%{buildroot}/usr headers_install
cp System.map %{buildroot}/boot/System.map-%{kbuildver}
cp .config %{buildroot}/boot/config-%{kbuildver}
bzip2 -9 --keep vmlinux
mv vmlinux.bz2 %{buildroot}/boot/vmlinux-%{kbuildver}
rm -f %{buildroot}/lib/modules/%{kbuildver}/build
rm -f %{buildroot}/lib/modules/%{kbuildver}/source
mkdir -p %{buildroot}/usr/src/kernels/%{kbuildver}
tar cf - --exclude SCCS --exclude BitKeeper --exclude .svn --exclude CVS --exclude .pc --exclude .hg --exclude .git --exclude=*vmlinux* --exclude=*.mod --exclude=*.o --exclude=*.ko --exclude=*.cmd --exclude=Documentation --exclude=.config.old --exclude=.missing-syscalls.d --exclude=*.s . | tar xf - -C %{buildroot}/usr/src/kernels/%{kbuildver}
cd %{buildroot}/lib/modules/%{kbuildver}
ln -sf /usr/src/kernels/%{kbuildver} build
ln -sf /usr/src/kernels/%{kbuildver} source

%clean
rm -rf %{buildroot}

%post
if [ -x /sbin/installkernel -a -r /boot/vmlinuz-%{kbuildver} -a -r /boot/System.map-%{kbuildver} ]; then
cp /boot/vmlinuz-%{kbuildver} /boot/.vmlinuz-%{kbuildver}-rpm
cp /boot/System.map-%{kbuildver} /boot/.System.map-%{kbuildver}-rpm
rm -f /boot/vmlinuz-%{kbuildver} /boot/System.map-%{kbuildver}
/sbin/installkernel %{kbuildver} /boot/.vmlinuz-%{kbuildver}-rpm /boot/.System.map-%{kbuildver}-rpm
rm -f /boot/.vmlinuz-%{kbuildver}-rpm /boot/.System.map-%{kbuildver}-rpm
fi
depmod -a %{kbuildver}
dracut -f %{kbuildver}

%preun
if [ -x /sbin/new-kernel-pkg ]; then
new-kernel-pkg --remove %{kbuildver} --rminitrd --initrdfile=/boot/initramfs-%{kbuildver}.img
elif [ -x /usr/bin/kernel-install ]; then
kernel-install remove %{kbuildver}
fi

%postun
if [ -x /sbin/update-bootloader ]; then
/sbin/update-bootloader --remove %{kbuildver}
fi

%files
%defattr (-, root, root)
/lib/modules/5.11.1-lqx1
%exclude /lib/modules/%{kbuildver}/build
%exclude /lib/modules/%{kbuildver}/source
/boot/*

%files headers
%defattr (-, root, root)
/usr/include

%files devel
%defattr (-, root, root)
/usr/src/kernels/%{kbuildver}
/lib/modules/%{kbuildver}/build
/lib/modules/%{kbuildver}/source
